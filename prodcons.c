#include <stdio.h>
#include <pthread.h>
#include "index.h"



void CleanupHandler(void *arg){
	pthread_mutex_unlock((pthread_mutex_t *)arg);
}

void AquireSemaphore(sema_t *ps){
	pthread_mutex_lock(&(ps->mutex));
	pthread_cleanup_push(CleanupHandler,&(ps->mutex));
	while(ps->count==0)
		pthread_cond_wait(&(ps->cond), &(ps->mutex));
	--ps->count;
	pthread_cleanup_pop(1);
}

void ReleaseSemaphore(sema_t *ps){
	pthread_mutex_lock(&(ps->mutex));
	pthread_cleanup_push(CleanupHandler,&(ps->mutex));
	++ps->count;
	pthread_cond_signal(&(ps->cond));
	pthread_cleanup_pop(1);
}



void *Producer(void* arg){
	while(1){
		AquireSemaphore(&buffer.empty);
		produce_item();
		ReleaseSemaphore(&buffer.full);
	}
}

void *Consumer(void *arg){
	while(1){
		AquireSemaphore(&buffer.full);
		consume_item();
		ReleaseSemaphore(&buffer.empty);
	}
}

void CreateSemaphore(sema_t *ps,int count){
	ps->count = count;
	pthread_mutex_init(&ps->mutex,NULL);
	pthread_cond_init(&ps->cond,NULL);
}


void consume_item(){
	printf("consuming *chomp chomp*\n");
}
void produce_item(){
	printf("producing *taka taka*\n");
}
