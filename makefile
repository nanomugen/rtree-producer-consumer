CC=gcc
CFLAGS=-g -W -Wall -fpermissive -O3 -lm -D_REENTRANT -lpthread

run: index.o rect.o card.o queue.o split_q.o node.o test.o
	$(CC) test.o index.o node.o rect.o card.o queue.o split_q.o -o run $(CFLAGS)

test.o: test.c index.h
	$(CC) -c test.c $(CFLAGS)

index.o: index.c index.h card.h
	$(CC) -c index.c $(CFLAGS)

node.o: node.c index.h card.h
	$(CC) -c node.c $(CFLAGS)

rect.o: rect.c index.h
	$(CC) -c rect.c $(CFLAGS)

queue.o: queue.c index.h
	$(CC) -c queue.c $(CFLAGS)

card.o: card.c index.h card.h
	$(CC) -c card.c $(CFLAGS)

split_q.o: split_q.c index.h card.h split_q.h
	$(CC) -c split_q.c $(CFLAGS)

clean:
	rm -rf *.o *.exe
