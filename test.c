
#include "index.h"
#include <stdio.h>
#include <math.h>




int MySearchCallback(int id, void* arg)
{
  // Note: -1 to make up for the +1 when data was inserted
  /* printf("%d>> Hit data rect %d\n",pthread_self(), id-1); */
  return 1; // keep going
}


int main(int argc, char **argv){
  //printf("Número de filhos por node: %d\n",MAXCARD);

  if(argc!=2){
    printf("digite o tamanho do vetor\n");
    exit(1);
  }

  int n_rects = atoi(*(argv+1));
  if(n_rects<=0){
    printf("INVALID INPUT\n" );
    exit(1);
  }

  struct Rect *rects = malloc(sizeof(struct Rect) * n_rects);
  int i, n_search1,n_search3,n_search4,n_search5;
  double t_search1, t_search3,t_search4,t_search5;
  clock_t start, end;
  threads = 0;
  total_threads = 0;
  struct Node* root = RTreeNewIndex();

  //GERADOR DOS VETORES
  for(i=0;i<n_rects;i++){
    int j;
    for(j=0;j<NUMSIDES;j++){
      if(j<NUMDIMS)
      rects[i].boundary[j]=i;
      else
      rects[i].boundary[j]=i+1;
    }

  }

  //vetor usado para parar os threads
  kill = malloc(sizeof(struct Node));
  kill->level=-1;

  /*
  * Insert all the data rects.
  * Notes about the arguments:
  * parameter 1 is the rect being inserted,
  * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
  * parameter 3 is the root of the tree. Note: its address is passed
  * because it can change as a result of this call, therefore no other parts
  * of this code should stash its address since it could change undernieth.
  * parameter 4 is always zero which means to add from the root.
  */
  for(i=0;i<n_rects;i++)
  RTreeInsertRect(&rects[i],i+1,&root,0);

  //rect a ser pesquisado
  struct Rect rect_search ;
  for(i=0;i<NUMSIDES/2;i++)
    rect_search.boundary[i]=0;

  for(i=NUMSIDES/2;i<NUMSIDES;i++)
    rect_search.boundary[i]=n_rects+1;

  //RTREESEARCH
  start = clock();
  n_search1 = RTreeSearch(root, &rect_search, MySearchCallback, 0);
  end = clock();
  t_search1 = (double)(end - start)/CLOCKS_PER_SEC;

  struct Search s_search1;
  s_search1.N=root;
  s_search1.R=&rect_search;
  s_search1.shcb=&MySearchCallback;
  s_search1.cbarg=NULL;
  n_search3=0;
  s_search1.hits = &n_search3;



  //RTREESEARCH3
  pthread_t thread_main;
  start = clock();
  pthread_create(&thread_main,NULL,RTreeSearch3,&s_search1);

  int teste_main;
  teste_main = pthread_join(thread_main,NULL);
  end = clock();
  t_search3 = (double)(end - start)/CLOCKS_PER_SEC;



  pthread_t thre[MAXTHR];
  struct Queue* q;
  QueueInit(&q);
  q->active= MAXTHR;
  struct Search s_search4[MAXTHR];
  int resp=0;
  for(i=0;i<MAXTHR;i++){
    s_search4[i].N=root;
    s_search4[i].R=&rect_search;
    s_search4[i].shcb=&MySearchCallback;
    s_search4[i].queue= &q;
    s_search4[i].cbarg=NULL;
    s_search4[i].hits=&n_search4;
    s_search4[i].flag=0;
    s_search4[i].depth = 0;
  }
  s_search4[0].flag=1;
  start= clock();
  for(i=0;i<MAXTHR;i++){
    pthread_create(&thre[i],NULL,RTreeSearch4,&s_search4[i]);
  }
  for(i=0;i<MAXTHR;i++)
  pthread_join(thre[i],NULL);
  end = clock();
  t_search4 = (double)(end - start)/CLOCKS_PER_SEC;


  //RTREESEARCH5
  Data s_search5[MAXTHR];
  n_search5=0;
  q->active= MAXTHR;
  q->inactive =0;
  for(i=0;i<MAXTHR;i++){
    s_search5[i].node=NULL;
    s_search5[i].rect = &rect_search;
    s_search5[i].queue = &q;
    s_search5[i].hits = &n_search5;
    pthread_create(&thre[i],NULL,InitThread,&s_search5[i]);
  }
  start=clock();
  QueuePush(&q,root);
  for(i=0;i<MAXTHR;i++){pthread_join(thre[i],NULL);}
  end = clock();
  t_search5 = (double)(end - start)/CLOCKS_PER_SEC;

  //printf("RTREESEARCH: %d | RTREESEARCH3: %d| RTREESEARCH4: %d| RTREESEARCH5: %d\n",n_search1,n_search3,n_search4,n_search5);
  //printf("RTREESEARCH: %.6lf | RTREESEARCH3: %.6lf| RTREESEARCH4: %.6lf| RTREESEARCH5: %.6lf\n", t_search1, t_search3,t_search4,t_search5 );
  printf("%.6lf\n%.6lf\n%.6lf\n%.6lf\n\n", t_search1, t_search3,t_search4,t_search5 );
  QueueKill(&q);

  return 0;
}
